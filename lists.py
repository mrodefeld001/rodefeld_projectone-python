#This is our plane program
#it holds planes and plane data
#helper functions for main for our python project1
#author McEwan Rodefeld
import sys
import os


def createList(filename):
    """this method creates a list from the csv file
    Parameters
    ----------
    the filename as a string

    Return
    ------
    the newly created list as an array
    """
    origin_list=[]
    in_file = open(filename, "r")
    origin_list = in_file.readlines()
    word = 0
    for y in origin_list:
       origin_list[word] = origin_list[word].strip('\n')
       word = word+1
    in_file.close()
    return origin_list
    

def createQueue(file_list):
    """This method creates a queue from the list based on requested time
    Parameters
    ----------
    the list that you want sorted as an array

    Return
    ------
    a queue as an array sorted by requested time
    """
    queue = []
    count = 0
    while count < len(file_list):
        queue = add(file_list[count], queue)
        count = count+1  
    queue = fancify(queue)
    return queue


def add(plane, plane_queue):
    """This adds a plane to the queue
    Parameters
    -----------
    plane:
        a plane object being added to the queue
    plane_queue:
        the queue of the plane
    Returns
    --------
    An array of planes
"""
    queue = []
    plane_queue.append(plane)
    plane_queue.sort(key = sorting)
    return plane_queue
    

def sorting(string):
    """this is for sorting the array of planes
    Parameters
    -----------
    string:
        a string to look at to sort

    Returns
    -------
        a new string for the array to sort with
    """
    new_string = string.split(",")
    return int(new_string[2])

    
def fancify(origin_list):
    """this is for making the "plane objects" appear nicely printed
    Parameters
    -----------
    origin_list:
        The list for this function to go through to make the plane objects appear nice

    Returns
    -------
        the list once it has been "fancified"
    """
    count = 0
    for x in origin_list:
        new_string = origin_list[count].split(",")
        plane_id = new_string[0]
        sub_time = new_string[1]
        req_time = new_string[2]
        req_dur = new_string[3]
        origin_list[count] = "Plane_id: " + plane_id + " Submission time: "+ sub_time + " Requested start: "+ req_time + " Requested Duration: " +req_dur
        count = count +1
    return origin_list


def runSimulation(origin_list):
    """This function is to run the time simulation for the planes and sort out when they
all go and will take them out of the queue and add them to the log
    Parameters:
    ------------
    origin_list:
        the queue to take the plane objects from

    Returns:
    ---------
    an array log of when all of the planes actually left in order
    """
    log=[]
    queue=[]
    current_time = 0
    duration_counter = 0
    index = 0
    on_deck = []
    while current_time < len(origin_list):
        if index != len(origin_list):
            on_deck = origin_list[index].split(",")
            while int(on_deck[1]) == current_time:

                queue = add(origin_list[index], queue)
                index = index + 1
                if index == len(origin_list):
                    break
                on_deck = origin_list[index].split(",")
        
            print(queue)
            #where I remove from the queue (will be in some sort of loop)
            first = queue[0]
            queue[0] = addTime(current_time, first)
            log = addLog(queue, log)
            queue.pop(0)


        current_time = current_time+1
    return log



def addLog(queue, log):
    """This function will add to the log
    Parameters:
    ------------
    queue:
        the queue to take the plane objects from
    log:
        the log to add to

    Returns:
    ---------
    an array log of when all of the planes actually left
    """
    next_up = queue[0]
    
    log.append(next_up)
    
    return log


def addTime(current_time, plane):
    """This function takes the current time that the plane wants to take off and
creates the time that the plane was on the runway to add to the log.

    Parameters:
    -----------
    current_time:
        the current time that the plane is starting on the runway
    plane:
        the plane item to get their time added on. in the log.

    Returns:
    ---------
    The new "plane" object with the time duration on the runway to be added to
    the log.
    """
    plane_object = []
    plane_object = plane.split(",")
    d = plane_object[3]
    duration = current_time + int(d)
    return_string = plane + " Duration: " + str(current_time) + "-" + str(duration)
    return return_string
    
    

        
