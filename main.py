# this is our main
# it will help run our lists program
# it will also read from a csv file
# author McEwan Rodefeld
import sys
import os
import lists

def main_class():
    """ This is out main class for the python project1"""
    origin_list = []
    origin_list = lists.createList("project1_test.csv")
    queue = []
    queue = lists.createQueue(origin_list)
    log = lists.runSimulation(origin_list)
    print()
    log = lists.fancify(log)
    print("LOG:")
    print(log)
    


#calling main loop
if __name__ == "__main__":
    main_class()
