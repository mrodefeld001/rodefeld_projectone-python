Intro:
the purpose of this project is to use Python to simulate aircraft takeoff
time slots at an airport. I will be keeping track of the plane's ID,
submission time, requested start, requested duration, actual start, and
actual end, while keeping track of them in "real time".
I will also be maintaining a queue that will hold all of this information.

Requirements:
I must have I will be keeping track of the plane's ID,
submission time, requested start time, requested duration, actual start,
and actual end time. I will also have to maintain a queue to hold all of
the planes. I will have to keep track of time so that the queue can be
updated according to the time. I will also have to be able to load
the plane's data from a file.





Author: McEwan Rodefeld
